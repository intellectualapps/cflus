CREATE TABLE `password_reset_token` (
  `email` VARCHAR(50) NOT NULL COMMENT '',
  `token` VARCHAR(45) NOT NULL COMMENT '',
  `expiry_time` DATE NOT NULL COMMENT '',
  PRIMARY KEY (`email`)  COMMENT '');

