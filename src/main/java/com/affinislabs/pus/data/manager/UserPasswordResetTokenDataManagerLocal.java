/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.data.manager;

import com.affinislabs.pus.model.UserPasswordResetToken;
/**
 *
 * @author buls
 */
public interface UserPasswordResetTokenDataManagerLocal {
 
    UserPasswordResetToken create(UserPasswordResetToken token);
    UserPasswordResetToken get(String email);
    void delete(UserPasswordResetToken token);
    
}