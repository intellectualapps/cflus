/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.pojo;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lateefah
 */
public class AppRole implements Serializable {
    
    private String roleId;
    private String description;
    private List<AppPrivilege> privileges;
    
    public AppRole() {
        privileges = new ArrayList<AppPrivilege>();
    }

    public AppRole(String roleId) {
        this.roleId = roleId;
    }

    public AppRole(String roleId, String description) {
        this.roleId = roleId;
        this.description = description;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AppPrivilege> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(List<AppPrivilege> privileges) {
        this.privileges = privileges;
    }
    
    public void setPrivilege(AppPrivilege privilege) {
        this.privileges.add(privilege);
    }

    public String getRolePrivilegesAsJson(){
        ObjectMapper jsonMapper = new ObjectMapper();
        String rolePrivilegesInString = "";
        try {
            rolePrivilegesInString = jsonMapper.writeValueAsString(getPrivileges());
        } catch (Exception e){
            
        }
        return rolePrivilegesInString;
    }
    
}
